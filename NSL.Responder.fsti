module NSL.Responder

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

val oracle_responder_receive_msg_3: nsl_state -> nat -> ciphertext -> Tot (nsl_state * option (unit))

val oracle_responder_send_msg_2: nsl_state -> address -> ciphertext -> Tot (nsl_state * option (nat * ciphertext))

