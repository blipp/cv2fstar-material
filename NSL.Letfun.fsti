module NSL.Letfun

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

val fun_enc : nsl_state -> plaintext -> pkey -> nsl_state * ciphertext_opt

val fun_keygen : nsl_state -> nsl_state * keypair

val fun_initiator_send_msg_1_inner : nsl_state -> address -> address -> nsl_state * msg1res
