module NSL.Key_Register

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol



let oracle_register state input_37 input_36 =

      let var_addr_2 = input_37 in
      let var_pkX_2 = input_36 in

          begin
            
            begin
            if entry_exists state Table_all_keys
              (fun (te:nsl_table_entry Table_all_keys) -> let TableEntry_all_keys tvar_40 tvar_39 tvar_38 = te in
                begin
            
                  let bvar_41 = tvar_40 in
                  let var_ign1_1 = tvar_39 in
                  let var_ign2_1 = tvar_38 in
                    if eq_addr bvar_41 var_addr_2 then Some () else None
                  
                end
              )then state, None else
              begin
                let state = insert state Table_all_keys (TableEntry_all_keys #Table_all_keys var_addr_2 var_pkX_2 false) in
                
                (state, Some ())
                
              end
            end
          end
              