module NSL.Setup

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol



let oracle_setup state input_11 =

      let var_addr_1 = input_11 in

      begin
      if entry_exists state Table_all_keys
        (fun (te:nsl_table_entry Table_all_keys) ->       let TableEntry_all_keys tvar_14 tvar_13 tvar_12 = te in
          begin

            let bvar_15 = tvar_14 in
            let var_ign1_0 = tvar_13 in
            let var_ign2_0 = tvar_12 in
              if eq_addr bvar_15 var_addr_1 then Some () else None
            
          end
        )      then state, None else
        begin
          
          let state, v16 = NSL.Letfun.fun_keygen state  in
          
          
          let bvar_17 = v16 in
            match inv_kp bvar_17 with
            | None ->   state, None
            | Some (var_the_pkA_0,var_the_skA_0) ->
          
              begin
                let state = insert state Table_trusted_keys (TableEntry_trusted_keys #Table_trusted_keys var_addr_1 var_the_skA_0 var_the_pkA_0) in
                let state = insert state Table_all_keys (TableEntry_all_keys #Table_all_keys var_addr_1 var_the_pkA_0 true) in
                
                (state, Some (var_the_pkA_0))
                
              end
            
        end
      end