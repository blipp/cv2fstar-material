open Types

(* Guess the tested session *)
  
val guess_session : guess_arg_t -> state -> game_transformer
