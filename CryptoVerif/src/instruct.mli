open Types

val do_proof : Ptree.command list option -> state -> unit

val initial_expand_simplify : state -> state
