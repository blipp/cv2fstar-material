#!/bin/sh
set -e

CWD=$(pwd)

DIR=$(basename "$CWD")
if [ "$DIR" != "distrib" ]
then
    echo "This script must be executed from within the 'distrib' directory."
    exit 2
fi

VERSION=$1
YEAR=`date +%Y`
DEST=$HOME/cryptoverifreleases
DESTD="$DEST"/cryptoverif"$VERSION"

if [ -z "$1" ]
then
    echo "Please indicate a version number as first argument."
    exit 2
fi


if [ ! -d "$DEST" ]
then
    echo "Directory '$DEST' does not exist. Please create it."
    exit 2
fi

if [ -d "$DESTD" ]
then
    echo "Directory '$DESTD' already exists. Please remove it."
    exit 2
fi

echo "Creating release of CryptoVerif version '$VERSION'"

cd ..
# We are now in the root directory of CryptoVerif.

set +e # Don't fail the script if there are no files to be removed.
rm *~ */*~ */*/*~
set -e

# Build header file
sed "s/YEAR/$YEAR/g" distrib/head.ml > distrib/head_year_tmp.ml

# Build src/version.ml
mv src/version.ml src/version.ml-tmp
echo "let version = \"$VERSION\"" > src/version.ml

# Build the manual
cd docs
pdflatex manual.tex
bibtex manual
pdflatex manual.tex
pdflatex manual.tex
cd ..

mkdir "$DESTD"

sed "s/VERSION/$VERSION/g;s/YEAR/$YEAR/g" README > "$DESTD"/README

mkdir "$DESTD"/cryptolib "$DESTD"/docs "$DESTD"/examples "$DESTD"/examples/basic "$DESTD"/examples/obasic "$DESTD"/examples/kerberos "$DESTD"/examples/hpke "$DESTD"/examples/arinc823 "$DESTD"/examples/arinc823/sharedkey "$DESTD"/examples/arinc823/publickey "$DESTD"/examples/textsecure "$DESTD"/examples/tls13 "$DESTD"/examples/wireguard "$DESTD"/src "$DESTD"/tests "$DESTD"/emacs "$DESTD"/implementation "$DESTD"/implementation/nspk "$DESTD"/implementation/wlsk "$DESTD"/implementation/ssh

for i in CHANGES LICENSE build build.bat test docs/manual.pdf emacs/* implementation/nspk/README implementation/wlsk/README implementation/ssh/README implementation/nspk/build.sh implementation/wlsk/build.sh implementation/ssh/build.sh implementation/ssh/prepare examples/arinc823/prepare examples/kerberos/prepare examples/kerberos/*.pcv examples/kerberos/*.m4.cv examples/hpke/README.md examples/hpke/prepare examples/hpke/common.*.ocvl examples/hpke/lib.*.ocvl examples/hpke/*.m4.ocv examples/hpke/keyschedule.auth.prf.ocv examples/textsecure/prepare examples/tls13/README.md examples/wireguard/boolean_choice.cvl examples/wireguard/preamble.cvl examples/wireguard/separator.cvl examples/wireguard/prepare src/rusage.c
do
    cp "$i" "$DESTD/$i"
done

for i in src/*.ml src/*.mli src/*.mll implementation/base.ml* implementation/crypto*.ml* implementation/nspk/A*.ml implementation/nspk/B*.ml implementation/nspk/S*.ml  implementation/nspk/test.ml implementation/nspk/nspk3tbl.ocv implementation/wlsk/init.ml implementation/wlsk/keygen.ml implementation/wlsk/resp.ml implementation/wlsk/server.ml implementation/wlsk/test_wlsk.ml implementation/wlsk/woolamskcorr_tbl.cv implementation/ssh/ssh*.ml* implementation/ssh/add_key.ml implementation/ssh/ssh.ocv implementation/ssh/ssh-secrecy-key.m4.ocv implementation/ssh/ssh-secrecy-key-indiff.ocv examples/basic/* examples/obasic/* examples/arinc823/arinc823prim.cvl examples/arinc823/*/*.m4.cv examples/arinc823/*/*.ocv examples/textsecure/*.m4.cv examples/textsecure/*.ocv examples/tls13/*.cv* examples/wireguard/hashgen.ml examples/wireguard/WG.25519.dos.cv examples/wireguard/*.m4.cv cryptolib/commonlib.cvl cryptolib/crypto.pvl cryptolib/*.ocv
do
    if [ -f "$i" ]
    then
        cat distrib/head_year_tmp.ml "$i" > "$DESTD"/"$i"
    fi
done

for i in src/*.mly
do
    if [ -f "$i" ]
    then
        (echo "%{"; cat distrib/head_year_tmp.ml; echo "%}"; cat "$i") > "$DESTD"/"$i"
    fi
done

cp "$DESTD"/implementation/crypto_real.ml "$DESTD"/implementation/crypto.ml

rm "$DESTD"/src/*lexer.ml "$DESTD"/src/*parser.ml "$DESTD"/src/*parser.mli "$DESTD"/src/profile.ml*

cd "$DEST"

tar -czf "$DEST"/cryptoverif"$VERSION".tar.gz cryptoverif"$VERSION"
rm -r "$DESTD"

cd "$CWD"/..

if uname -a | grep -E -q \(Cygwin\)\|\(MINGW\)
then

    echo "Creating binary release of CryptoVerif version '$VERSION'."
    
    # We are now in the root directory of CryptoVerif.
    mkdir "$DESTD"
    
    # Build executables
    ./build
    
    # Build wireguard.cvl
    cd examples/wireguard
    ./prepare
    cd ../..

    sed "s/VERSION/$VERSION/g;s/YEAR/$YEAR/g" distrib/READMEBIN > "$DESTD"/READMEBIN
    
    mkdir "$DESTD"/docs "$DESTD"/examples "$DESTD"/examples/basic "$DESTD"/examples/obasic "$DESTD"/examples/kerberos "$DESTD"/examples/hpke "$DESTD"/examples/arinc823 "$DESTD"/examples/arinc823/sharedkey "$DESTD"/examples/arinc823/publickey "$DESTD"/examples/textsecure "$DESTD"/examples/tls13 "$DESTD"/examples/wireguard "$DESTD"/tests "$DESTD"/emacs "$DESTD"/implementation "$DESTD"/implementation/nspk "$DESTD"/implementation/wlsk "$DESTD"/implementation/ssh
    
    for i in CHANGES LICENSE test cryptoverif.exe cryptogen.exe analyze.exe addexpectedtags.exe docs/manual.pdf emacs/* implementation/nspk/README implementation/wlsk/README implementation/ssh/README implementation/nspk/build.sh implementation/wlsk/build.sh implementation/ssh/build.sh implementation/ssh/prepare examples/arinc823/prepare examples/kerberos/prepare examples/kerberos/*.pcv examples/kerberos/*.m4.cv examples/hpke/README.md examples/hpke/prepare examples/hpke/common.*.ocvl examples/hpke/lib.*.ocvl examples/hpke/*.m4.ocv examples/hpke/keyschedule.auth.prf.ocv examples/textsecure/prepare examples/tls13/README.md examples/wireguard/prepare 
    do
	cp "$i" "$DESTD/$i"
    done

    for i in default.cvl default.ocvl cryptoverif.pvl implementation/base.ml* implementation/crypto*.ml* implementation/nspk/A*.ml implementation/nspk/B*.ml implementation/nspk/S*.ml  implementation/nspk/test.ml implementation/nspk/nspk3tbl.ocv implementation/wlsk/init.ml implementation/wlsk/keygen.ml implementation/wlsk/resp.ml implementation/wlsk/server.ml implementation/wlsk/test_wlsk.ml implementation/wlsk/woolamskcorr_tbl.cv implementation/ssh/ssh*.ml* implementation/ssh/add_key.ml implementation/ssh/ssh.ocv implementation/ssh/ssh-secrecy-key.m4.ocv implementation/ssh/ssh-secrecy-key-indiff.ocv examples/basic/* examples/obasic/* examples/arinc823/arinc823prim.cvl examples/arinc823/*/*.m4.cv examples/arinc823/*/*.ocv examples/textsecure/*.m4.cv examples/textsecure/*.ocv examples/tls13/*.cv* examples/wireguard/mylib.cvl examples/wireguard/WG.25519.dos.cv examples/wireguard/*.m4.cv 
    do
	if [ -f "$i" ]
	then
	    cat distrib/head_year_tmp.ml "$i" > "$DESTD"/"$i"
	fi
    done

    cp "$DESTD"/implementation/crypto_real.ml "$DESTD"/implementation/crypto.ml
    
    cd "$DEST"
    
    zip -r "$DEST"/cryptoverifbin"$VERSION".zip cryptoverif"$VERSION"
    rm -r "$DESTD"
    
else
    echo Could not build Windows binary distribution.
    echo You need to be under cygwin or mingw for that.
fi

# Remove temporary files
cd "$CWD"/..
mv src/version.ml-tmp src/version.ml
rm distrib/head_year_tmp.ml

cd "$CWD"
