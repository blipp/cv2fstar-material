module NSL.Sessions

open CVTypes
open NSL.Types
open State

let nsl_print_session_entry se =
  match se with
  | R1_responder_receive_msg_3 var_Na_4 var_Nb_2 var_addrB_0 var_addrY_0 var_skB_0 var_trustY_0  ->
    IO.print_string "R1_responder_receive_msg_3: {\n";
    print_label_bytes "var_Na_4" (serialize_lbytes var_Na_4) true;
    print_label_bytes "var_Nb_2" (serialize_lbytes var_Nb_2) true;
    print_label_bytes "var_addrB_0" (serialize_address var_addrB_0) true;
    print_label_bytes "var_addrY_0" (serialize_address var_addrY_0) true;
    print_label_bytes "var_skB_0" (serialize_skey var_skB_0) true;
    print_label_bytes "var_trustY_0" (serialize_bool var_trustY_0) true;
    IO.print_string "}\n"
  | R1_initiator_send_msg_3 var_Na_3 var_addrA_1 var_addrX_1 var_pkX_4 var_skA_2 var_trustX_2  ->
    IO.print_string "R1_initiator_send_msg_3: {\n";
    print_label_bytes "var_Na_3" (serialize_lbytes var_Na_3) true;
    print_label_bytes "var_addrA_1" (serialize_address var_addrA_1) true;
    print_label_bytes "var_addrX_1" (serialize_address var_addrX_1) true;
    print_label_bytes "var_pkX_4" (serialize_pkey var_pkX_4) true;
    print_label_bytes "var_skA_2" (serialize_skey var_skA_2) true;
    print_label_bytes "var_trustX_2" (serialize_bool var_trustX_2) true;
    IO.print_string "}\n"
  | R1_initiator_finish var_Na_3 var_Nb_1 var_addrA_1 var_addrX_1 var_trustX_2  ->
    IO.print_string "R1_initiator_finish: {\n";
    print_label_bytes "var_Na_3" (serialize_lbytes var_Na_3) true;
    print_label_bytes "var_Nb_1" (serialize_lbytes var_Nb_1) true;
    print_label_bytes "var_addrA_1" (serialize_address var_addrA_1) true;
    print_label_bytes "var_addrX_1" (serialize_address var_addrX_1) true;
    print_label_bytes "var_trustX_2" (serialize_bool var_trustX_2) true;
    IO.print_string "}\n"
