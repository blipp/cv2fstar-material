module NSL.Protocol

open CVTypes
open NSL.Types
open NSL.Tables
open NSL.Sessions
open NSL.Events
open State

let nsl_state_type = StateType nsl_table_type nsl_session_type nsl_event

type nsl_state = state nsl_state_type

let nsl_print_session (st: nsl_state) (sid: nat) : FStar.All.ML unit =
  print_session #nsl_state_type st nsl_print_session_entry sid
