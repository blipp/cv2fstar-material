module NSL.Responder

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol



let oracle_responder_receive_msg_3 state sid input_18 =
match get_and_remove_session_entry state Oracle_responder_receive_msg_3 sid with
| state, None -> state, None
| state, Some (se:nsl_session_entry) ->
  match se with
    | R1_responder_receive_msg_3 var_Na_4 var_Nb_2 var_addrB_0 var_addrY_0 var_skB_0 var_trustY_0 ->

      let var_m3_0 = input_18 in


      
      let bvar_19 = (dec var_m3_0 var_skB_0) in
        match inv_injbot bvar_19 with
        | None ->   state, None
        | Some (bvar_20) ->
        match inv_msg3 bvar_20 with
        | None ->   state, None
        | Some (bvar_21) ->
        if eq_lbytes bvar_21 var_Nb_2 then
          begin
            
            
            if var_trustY_0 then
            begin
              let ev = Event_endB var_addrY_0 var_addrB_0 var_Na_4 var_Nb_2 in
            let state = state_add_event state ev in
            
            (state, Some ())
            
            end
            else
            begin
            (state, Some ())
            
            end
          end
        else state, None
      

let oracle_responder_send_msg_2 state input_23 input_22 =
let state, sid = state_reserve_session_id state in

      let var_addrB_0 = input_23 in
      let var_m_0 = input_22 in

          begin
            
            begin
            match get_unique state Table_trusted_keys
              (fun (te:nsl_table_entry Table_trusted_keys) -> let TableEntry_trusted_keys tvar_26 tvar_25 tvar_24 = te in
                begin
            
                  let bvar_27 = tvar_26 in
                  let var_skB_0 = tvar_25 in
                  let var_pkB_0 = tvar_24 in
                    if eq_addr bvar_27 var_addrB_0 then
                      begin
                        Some (var_skB_0)
                      end
                    else None
                  
                end
              )
            with
            | None -> state, None
            | Some (var_skB_0) ->
              begin
                
                
                
                let bvar_28 = (dec var_m_0 var_skB_0) in
                  match inv_injbot bvar_28 with
                  | None ->   state, None
                  | Some (bvar_29) ->
                  match inv_msg1 bvar_29 with
                  | None ->   state, None
                  | Some (var_Na_4,var_addrY_0) ->
                
                    begin
                      
                      begin
                      match get_unique state Table_all_keys
                        (fun (te:nsl_table_entry Table_all_keys) -> let TableEntry_all_keys tvar_32 tvar_31 tvar_30 = te in
                          begin
                      
                            let bvar_33 = tvar_32 in
                            let var_pkY_0 = tvar_31 in
                            let var_trustY_0 = tvar_30 in
                              if eq_addr bvar_33 var_addrY_0 then
                                begin
                                  Some (var_trustY_0,var_pkY_0)
                                end
                              else None
                            
                          end
                        )
                      with
                      | None -> state, None
                      | Some (var_trustY_0,var_pkY_0) ->
                        begin
                          
                          let state, var_Nb_2 = call_with_entropy state (gen_lbytes 8) in
                          
                          let ev = Event_beginB var_addrY_0 var_addrB_0 var_Na_4 var_Nb_2 in
                          let state = state_add_event state ev in
                          
                          let state, v34 = NSL.Letfun.fun_enc state (msg2 var_Na_4 var_Nb_2 var_addrB_0) var_pkY_0 in
                          
                          
                          let bvar_35 = v34 in
                            match inv_ciphertext_some bvar_35 with
                            | None ->   state, None
                            | Some (var_c2_0) ->
                          
                              begin
                                let sel = [R1_responder_receive_msg_3 var_Na_4 var_Nb_2 var_addrB_0 var_addrY_0 var_skB_0 var_trustY_0] in
                                let state = state_add_to_session state sid sel in
                                
                                (state, Some (sid, var_c2_0))
                                
                              end
                            
                        end
                      end
                    end
                  
              end
            end
          end
              