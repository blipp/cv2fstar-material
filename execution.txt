## Tables After Calling Setup

trusted_keys [
{
  address: 41406c6f63616c686f7374,
  skey: 53cb277a99b5cb5e5415c70cba5253608e094fe241b30ab85b2d4c6a8f7eb12b,
  pkey: 6d7c8fe400c016674e62d84fab09ce7fd61b83d32324bf2825fd337d9d71a301,
}
{
  address: 42406c6f63616c686f7374,
  skey: d9f3f877c463f5a73615880de60f73dfe46df8a6dccc088306f602b1255a0141,
  pkey: fd91a8ca37dac95a1c17a766a4ee2461de2bb7f4da628cffe99e926eca380060,
}
]

all_keys [
{
  address: 41406c6f63616c686f7374,
  pkey: 6d7c8fe400c016674e62d84fab09ce7fd61b83d32324bf2825fd337d9d71a301,
  bool: ff,
}
{
  address: 42406c6f63616c686f7374,
  pkey: fd91a8ca37dac95a1c17a766a4ee2461de2bb7f4da628cffe99e926eca380060,
  bool: ff,
}
]


## Sessions After init_send_msg_1

0.R1_initiator_send_msg_3: {
  var_Na_3: 9bcae7aeb3fb1085,
  var_addrA_1: 41406c6f63616c686f7374,
  var_addrX_1: 42406c6f63616c686f7374,
  var_pkX_4: fd91a8ca37dac95a1c17a766a4ee2461de2bb7f4da628cffe99e926eca380060,
  var_skA_2: 53cb277a99b5cb5e5415c70cba5253608e094fe241b30ab85b2d4c6a8f7eb12b,
  var_trustX_2: ff,
}


## Protocol Message 1

  ct: adf4368b100ff6675c80ee04ed4667c5c88f489b72a3ddd50402b3089eccbe250cf857491f27ea9614bc0c5e326cb52f4f82703b6d5996df8643a75ada69bd7dccca06f43883,


## Sessions After resp_send_msg_2

0.R1_initiator_send_msg_3: {
  var_Na_3: 9bcae7aeb3fb1085,
  var_addrA_1: 41406c6f63616c686f7374,
  var_addrX_1: 42406c6f63616c686f7374,
  var_pkX_4: fd91a8ca37dac95a1c17a766a4ee2461de2bb7f4da628cffe99e926eca380060,
  var_skA_2: 53cb277a99b5cb5e5415c70cba5253608e094fe241b30ab85b2d4c6a8f7eb12b,
  var_trustX_2: ff,
}
1.R1_responder_receive_msg_3: {
  var_Na_4: 9bcae7aeb3fb1085,
  var_Nb_2: 3ac19b9d052e94f5,
  var_addrB_0: 42406c6f63616c686f7374,
  var_addrY_0: 41406c6f63616c686f7374,
  var_skB_0: d9f3f877c463f5a73615880de60f73dfe46df8a6dccc088306f602b1255a0141,
  var_trustY_0: ff,
}


## Protocol Message 2

  ct: d83c39ad7d4701a4d2462f9423667a32d88d64c31b27d8e326f325c5d5f89f09b67676a8494a80eaedf17e5741d01b619992fd27e0153980c6bad1f8277c43cb834c70391d0c697adde9f7a2acaf,


## Sessions After init_send_msg_3

0.R1_initiator_finish: {
  var_Na_3: 9bcae7aeb3fb1085,
  var_Nb_1: 3ac19b9d052e94f5,
  var_addrA_1: 41406c6f63616c686f7374,
  var_addrX_1: 42406c6f63616c686f7374,
  var_trustX_2: ff,
}
1.R1_responder_receive_msg_3: {
  var_Na_4: 9bcae7aeb3fb1085,
  var_Nb_2: 3ac19b9d052e94f5,
  var_addrB_0: 42406c6f63616c686f7374,
  var_addrY_0: 41406c6f63616c686f7374,
  var_skB_0: d9f3f877c463f5a73615880de60f73dfe46df8a6dccc088306f602b1255a0141,
  var_trustY_0: ff,
}


## Protocol Message 3

  ct: 48730c365da9350e69da4230e91bfb12db40b444177ada11ecdf8ef6493b985557bd1cd4b3e99e647e0d82be740ed11e0c6771ead7ab9d0560,


## Sessions After resp_recv_msg_3

0.R1_initiator_finish: {
  var_Na_3: 9bcae7aeb3fb1085,
  var_Nb_1: 3ac19b9d052e94f5,
  var_addrA_1: 41406c6f63616c686f7374,
  var_addrX_1: 42406c6f63616c686f7374,
  var_trustX_2: ff,
}
1.session does not exist


## Sessions After init_finish

0.session does not exist
1.session does not exist


## Event List at the End of Protocol Execution

Events: [
beginB: {   address: 41406c6f63616c686f7374,
  address: 42406c6f63616c686f7374,
  lbytes 8: 9bcae7aeb3fb1085,
  lbytes 8: 3ac19b9d052e94f5,
}
beginA: {   address: 41406c6f63616c686f7374,
  address: 42406c6f63616c686f7374,
  lbytes 8: 9bcae7aeb3fb1085,
  lbytes 8: 3ac19b9d052e94f5,
}
endB: {   address: 41406c6f63616c686f7374,
  address: 42406c6f63616c686f7374,
  lbytes 8: 9bcae7aeb3fb1085,
  lbytes 8: 3ac19b9d052e94f5,
}
endA: {   address: 41406c6f63616c686f7374,
  address: 42406c6f63616c686f7374,
  lbytes 8: 9bcae7aeb3fb1085,
  lbytes 8: 3ac19b9d052e94f5,
}
]

success
