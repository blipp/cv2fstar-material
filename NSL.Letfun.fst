module NSL.Letfun

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

let fun_enc (state:nsl_state) (var_m_1 : plaintext) (var_pk_0 : pkey) =
(
  let state, var_r_0 = call_with_entropy state (gen_lbytes 32) in
(state, (enc var_m_1 var_pk_0 var_r_0)))

let fun_keygen (state:nsl_state)  =
(
  let state, var_k_0 = call_with_entropy state (gen_lbytes 32) in
(state, (kp (pkgen var_k_0) (skgen var_k_0))))

let fun_initiator_send_msg_1_inner (state:nsl_state) (var_addrA_0 : address) (var_addrX_0 : address) =

  begin
  match get_unique state Table_trusted_keys
    (fun (te:nsl_table_entry Table_trusted_keys) ->   let TableEntry_trusted_keys tvar_3 tvar_2 tvar_1 = te in
      begin

        let bvar_4 = tvar_3 in
        let var_skA_0 = tvar_2 in
        let var_pkA_0 = tvar_1 in
          if eq_addr bvar_4 var_addrA_0 then
            begin
              Some (var_skA_0)
            end
          else None
        
      end
    )
  with
  | None ->
    begin
      (state, msg1fail)
    end
  | Some (var_skA_0) ->
    begin
      
      begin
      match get_unique state Table_all_keys
        (fun (te:nsl_table_entry Table_all_keys) -> let TableEntry_all_keys tvar_7 tvar_6 tvar_5 = te in
          begin
      
            let bvar_8 = tvar_7 in
            let var_pkX_0 = tvar_6 in
            let var_trustX_0 = tvar_5 in
              if eq_addr bvar_8 var_addrX_0 then
                begin
                  Some (var_trustX_0,var_pkX_0)
                end
              else None
            
          end
        )
      with
      | None ->
        begin
          (state, msg1fail)
        end
      | Some (var_trustX_0,var_pkX_0) ->
        begin
          (
          let state, var_Na_0 = call_with_entropy state (gen_lbytes 8) in
          (let state, v9 = fun_enc state (msg1 var_Na_0 var_addrA_0) var_pkX_0 in
          
          let var_cc1_0 = v9 in
          (state, (  let bvar_10 = var_cc1_0 in
            match inv_ciphertext_some bvar_10 with
            | None ->   msg1fail
            | Some (var_c1_0) ->
          
              begin
                (msg1succ var_skA_0 var_pkX_0 var_trustX_0 var_Na_0 var_c1_0)
              end
            ))))
        end
      end
    end
  end
