module NSL.Key_Register

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

val oracle_register: nsl_state -> address -> pkey -> Tot (nsl_state * option (unit))

