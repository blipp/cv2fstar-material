module NSL.Setup

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

val oracle_setup: nsl_state -> address -> Tot (nsl_state * option (pkey))

