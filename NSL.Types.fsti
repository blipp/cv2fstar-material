module NSL.Types

open CVTypes

val msg1res: Type0
val ciphertext_opt: Type0
val plaintext: Type0
val ciphertext: Type0
val skey: Type0
val keypair: Type0
val pkey: Type0
val address: Type0


val eq_msg1res: msg1res -> msg1res -> bool

val eq_ciphertext_opt: ciphertext_opt -> ciphertext_opt -> bool

val eq_plaintext: plaintext -> plaintext -> bool

val eq_ciphertext: ciphertext -> ciphertext -> bool

val eq_skey: skey -> skey -> bool

val eq_pkey: pkey -> pkey -> bool

val eq_keypair: keypair -> keypair -> bool

val eq_addr: address -> address -> bool



val serialize_ciphertext: ciphertext -> bytes
val deserialize_ciphertext: bytes -> option (ciphertext)

val serialize_skey: skey -> bytes
val deserialize_skey: bytes -> option (skey)

val serialize_pkey: pkey -> bytes
val deserialize_pkey: bytes -> option (pkey)

val serialize_address: address -> bytes
val deserialize_address: bytes -> option (address)



