module NSL.Initiator

open CVTypes
open State
open Helper
open NSL.Types
open NSL.Functions
open NSL.Tables
open NSL.Sessions
open NSL.Events
open NSL.Protocol

val oracle_initiator_finish: nsl_state -> nat -> Tot (nsl_state * option (unit))

val oracle_initiator_send_msg_1: nsl_state -> address -> address -> Tot (nsl_state * option (nat * ciphertext))

val oracle_initiator_send_msg_3: nsl_state -> nat -> ciphertext -> Tot (nsl_state * option (ciphertext))

