module NSL.Sessions

open CVTypes
open NSL.Types
open State

type nsl_oracle_name: eqtype =
  | Oracle_setup
  | Oracle_responder_receive_msg_3
  | Oracle_responder_send_msg_2
  | Oracle_register
  | Oracle_initiator_finish
  | Oracle_initiator_send_msg_1
  | Oracle_initiator_send_msg_3


noeq type nsl_session_entry =
  | R1_responder_receive_msg_3: var_Na_4:lbytes 8 -> var_Nb_2:lbytes 8 -> var_addrB_0:address -> var_addrY_0:address -> var_skB_0:skey -> var_trustY_0:bool -> nsl_session_entry
  | R1_initiator_send_msg_3: var_Na_3:lbytes 8 -> var_addrA_1:address -> var_addrX_1:address -> var_pkX_4:pkey -> var_skA_2:skey -> var_trustX_2:bool -> nsl_session_entry
  | R1_initiator_finish: var_Na_3:lbytes 8 -> var_Nb_1:lbytes 8 -> var_addrA_1:address -> var_addrX_1:address -> var_trustX_2:bool -> nsl_session_entry


let nsl_session_entry_to_name se =
  match se with
  | R1_responder_receive_msg_3 _ _ _ _ _ _ -> Oracle_responder_receive_msg_3
  | R1_initiator_send_msg_3 _ _ _ _ _ _ -> Oracle_initiator_send_msg_3
  | R1_initiator_finish _ _ _ _ _ -> Oracle_initiator_finish


let nsl_following_oracles on =
  match on with
  | Oracle_setup -> []
  | Oracle_responder_receive_msg_3 -> []
  | Oracle_responder_send_msg_2 -> [Oracle_responder_receive_msg_3]
  | Oracle_register -> []
  | Oracle_initiator_finish -> []
  | Oracle_initiator_send_msg_1 -> [Oracle_initiator_send_msg_3]
  | Oracle_initiator_send_msg_3 -> [Oracle_initiator_finish]


val nsl_print_session_entry: nsl_session_entry -> FStar.All.ML unit

let nsl_session_type = SessionType nsl_oracle_name nsl_session_entry nsl_following_oracles nsl_session_entry_to_name

