FSTAR_HOME ?= /Users/bhargava/Desktop/repositories/fstar
CV2FSTAR_HOME ?= /Users/bhargava/Desktop/repositories/cv2fstar/fstar_manual
HACL_HOME ?= /Users/bhargava/Desktop/repositories/hacl-star-master

CACHE_DIR     ?= $(CV2FSTAR_HOME)/.cache
HINT_DIR      ?= $(CV2FSTAR_HOME)/.hints
OCAML_DIR     ?= $(CV2FSTAR_HOME)/ocaml

.PHONY: all verify extract test clean distclean test_hpke

all:
	rm -f .depend && $(MAKE) .depend
	$(MAKE) verify

include $(HACL_HOME)/Makefile.include

HACL_FST_FILES = Spec.Loops.fst Lib.IntTypes.fst Lib.ByteSequence.fst Lib.Sequence.fst Spec.Agile.AEAD.fst Spec.Agile.HKDF.fst Spec.Hash.Definitions.fst Spec.Agile.Hash.fst Lib.LoopCombinators.fst Spec.Ed25519.fst Spec.Agile.DH.fst Spec.Agile.HPKE.fst Spec.Agile.HMAC.fst

#HACL_CMX_FILES = $(OCAML_DIR)/Spec_Loops.cmx $(OCAML_DIR)/Lib_IntTypes.cmx $(OCAML_DIR)/Lib_ByteSequence.cmx $(OCAML_DIR)/Lib_Sequence.cmx $(OCAML_DIR)/Spec_Agile_AEAD.cmx $(OCAML_DIR)/Spec_Agile_HKDF.cmx $(OCAML_DIR)/Spec_Hash_Definitions.cmx $(OCAML_DIR)/Spec_Agile_Hash.cmx $(OCAML_DIR)/Lib_LoopCombinators.cmx $(OCAML_DIR)/Spec_Ed25519.cmx $(OCAML_DIR)/Spec_Agile_DH.cmx $(OCAML_DIR)/Spec_Agile_HPKE.cmx $(OCAML_DIR)/Spec_Agile_HMAC.cmx
# Spec_Ed25519.cmx is the only one I don't have

HACL_CMX_FILES = $(addprefix $(OCAML_DIR)/,Lib_IntTypes.cmx Lib_LoopCombinators.cmx Lib_Sequence.cmx Lib_ByteSequence.cmx Lib_PrintSequence.cmx Lib_RandomSequence.cmx \
                 Lib_UpdateMulti.cmx Spec_Loops.cmx Spec_GaloisField.cmx Spec_Blake2.cmx Spec_Hash_Definitions.cmx Spec_Hash_PadFinish.cmx Spec_MD5.cmx Spec_SHA2_Constants.cmx Spec_SHA2.cmx Spec_P256_Definitions.cmx Spec_P256_Lemmas.cmx Spec_SHA1.cmx \
                 Spec_ECDSAP256_Definition.cmx Spec_Chacha20.cmx Spec_P256.cmx Spec_AES.cmx Spec_Agile_Hash.cmx Spec_Poly1305.cmx \
                 Spec_Chacha20Poly1305.cmx Spec_ECDSA.cmx Spec_Agile_Cipher.cmx Spec_Agile_HMAC.cmx \
                 Spec_Agile_AEAD.cmx Spec_Agile_HKDF.cmx Spec_P256.cmx Spec_DH.cmx Spec_Curve25519.cmx \
                 Spec_Agile_DH.cmx Spec_Agile_HPKE.cmx Spec_HMAC_DRBG.cmx)

# Unused at the moment, was just a quick try. Set to empty on purpose.
VALE_CMX_FILES = #$(addprefix $(OCAML_DIR)/,Vale_AES_AES_s.cmx)


# This is VALE_DIRS from hacl-star/Makefile.include, but in a different order
VALE_DIRS	= \
  $(HACL_HOME)/vale/specs/crypto \
  $(HACL_HOME)/vale/specs/defs \
  $(HACL_HOME)/vale/specs/hardware \
  $(HACL_HOME)/vale/specs/interop \
  $(HACL_HOME)/vale/specs/math \
  $(HACL_HOME)/vale/code/crypto/aes \
  $(HACL_HOME)/vale/code/arch \
  $(HACL_HOME)/vale/code/arch/x64 \
  $(HACL_HOME)/vale/code/arch/x64/interop \
  $(HACL_HOME)/vale/code/crypto/aes/x64 \
  $(HACL_HOME)/vale/code/crypto/bignum \
  $(HACL_HOME)/vale/code/crypto/ecc/curve25519 \
  $(HACL_HOME)/vale/code/crypto/poly1305 \
  $(HACL_HOME)/vale/code/crypto/poly1305/x64 \
  $(HACL_HOME)/vale/code/crypto/sha \
  $(HACL_HOME)/vale/code/lib/collections \
  $(HACL_HOME)/vale/code/lib/math \
  $(HACL_HOME)/vale/code/lib/util \
  $(HACL_HOME)/vale/code/lib/util/x64 \
  $(HACL_HOME)/vale/code/lib/util/x64/stdcalls \
  $(HACL_HOME)/vale/code/lib/transformers \
  $(HACL_HOME)/vale/code/test \
  $(HACL_HOME)/vale/code/thirdPartyPorts/Intel/aes/x64 \
  $(HACL_HOME)/vale/code/thirdPartyPorts/OpenSSL/aes \
  $(HACL_HOME)/vale/code/thirdPartyPorts/OpenSSL/poly1305/x64 \
  $(HACL_HOME)/vale/code/thirdPartyPorts/OpenSSL/sha \
  $(HACL_HOME)/vale/code/thirdPartyPorts/rfc7748/curve25519/x64 \
  $(HACL_HOME)/vale/code/thirdPartyPorts/SymCrypt/bignum

FSTAR_INCLUDE_DIRS = $(HACL_HOME)/lib $(HACL_HOME)/specs $(HACL_HOME)/specs/drbg $(HACL_HOME)/specs/lemmas $(HACL_HOME)/specs/ecdsap256 $(KREMLIN_HOME)/kremlib $(VALE_DIRS) $(KREMLIN_HOME)/kremlib $(CV2FSTAR_HOME)

FSTAR_FLAGS = --cmi \
  --cache_checked_modules --cache_dir $(CACHE_DIR) \
  --already_cached "+Prims+FStar+LowStar+C+TestLib+Spec+Lib+Vale" \
  $(addprefix --include ,$(FSTAR_INCLUDE_DIRS))

FSTAR = $(FSTAR_HOME)/bin/fstar.exe $(FSTAR_FLAGS) $(OTHERFLAGS)

ENABLE_HINTS = --use_hints --use_hint_hashes --record_hints # --query_stats

ALL_SOURCES = $(wildcard *.fst) $(wildcard *.fsti)

FIXME =

ROOTS = $(filter-out $(FIXME),$(ALL_SOURCES))

# --odir $(OCAML_DIR) is important here, because it will match with the $(OCAML_DIR)/%.ml
# targets and properly trigger a new extracting iff necessary.
.depend:
	$(info $(ROOTS))
	$(FSTAR) --cmi --dep full --odir $(OCAML_DIR) $(ROOTS) --extract '* -Lib -Spec -Vale -Prims -LowStar -FStar' > $@

include .depend

$(HINT_DIR):
	mkdir -p $@

$(CACHE_DIR):
	mkdir -p $@

$(OCAML_DIR):
	mkdir -p $@

# Add --query_stats to the FStar line for debugging purposes
$(CACHE_DIR)/%.checked: | .depend $(HINT_DIR) $(CACHE_DIR)
	$(FSTAR) --query_stats $< $(ENABLE_HINTS) --hint_file $(HINT_DIR)/$(notdir $*).hints

verify: $(addsuffix .checked, $(addprefix $(CACHE_DIR)/,$(ROOTS)))

# Targets for interactive mode

%.fst-in:
	@echo $(FSTAR_FLAGS) \
	  $(ENABLE_HINTS) --hint_file $(HINT_DIR)/$(basename $@).fst.hints

%.fsti-in:
	@echo $(FSTAR_FLAGS) \
	  $(ENABLE_HINTS) --hint_file $(HINT_DIR)/$(basename $@).fsti.hints

#OCaml extraction
CODEGEN=OCaml
CODEGEN_CMD1 = $(FSTAR) --codegen $(CODEGEN) --extract_module
CODEGEN_CMD2 = --odir $(OCAML_DIR)

include Makefile.Protocol

ALL_CMX_FILES=$(HACL_CMX_FILES) $(PROTO_CMX_FILES)

ifeq ($(OS),Windows_NT)
  export OCAMLPATH := $(FSTAR_HOME)/bin;$(OCAMLPATH)
else
  export OCAMLPATH := $(FSTAR_HOME)/bin:$(OCAMLPATH)
endif

OCAMLOPT = ocamlfind opt -package fstarlib -package cryptokit -linkpkg -linkall -g -I $(HACL_HOME)/obj -I $(OCAML_DIR) -w -8-20-26
OCAMLSHARED = ocamlfind opt -shared -package fstar-tactics-lib -package cryptokit -g -I $(HACL_HOME)/obj -I $(OCAML_DIR) -w -8-20-26
#OCAMLOPT = ocamlfind opt -package fstarlib -package cryptokit -linkpkg -linkall -g -I $(OCAML_DIR) -w -8-20-26
#OCAMLSHARED = ocamlfind opt -shared -package fstar-tactics-lib -package cryptokit -g -I $(OCAML_DIR) -w -8-20-26

extract: $(ALL_CMX_FILES)

test: $(OCAML_DIR)/test.exe
	$(OCAML_DIR)/test.exe

#$(OCAML_DIR)/test.exe: $(OCAML_DIR)/hacl.cmxa $(PROTO_CMX_FILES)
$(OCAML_DIR)/test.exe: $(HACL_HOME)/obj/libhaclml.cmxa $(OCAML_DIR)/Lib_RandomSequence.cmx $(PROTO_CMX_FILES)
	$(OCAMLOPT) $^ -o $@

test_hpke: $(OCAML_DIR)/test_hpke.exe
	$(OCAML_DIR)/test_hpke.exe

#$(OCAML_DIR)/test_hpke.exe: $(OCAML_DIR)/hacl.cmxa Test_Spec_Agile_HPKE.ml
$(OCAML_DIR)/test_hpke.exe: $(HACL_HOME)/obj/libhaclml.cmxa Test_Spec_Agile_HPKE.ml
	$(OCAMLOPT) $^ -o $@

.PRECIOUS: $(OCAML_DIR)/%.ml

$(OCAML_DIR)/Spec_HMAC_DRBG.ml: | $(OCAML_DIR)
	$(CODEGEN_CMD1) Spec.HMAC_DRBG $(CODEGEN_CMD2) Spec.HMAC_DRBG.fst

#$(OCAML_DIR)/Vale_AES_AES_s.ml: | $(OCAML_DIR)
#	$(CODEGEN_CMD1) Vale.AES.AES_s $(CODEGEN_CMD2) Vale.AES.AES_s.fst

# This rule has prerequisites coming from .depend!
$(OCAML_DIR)/%.ml: | $(OCAML_DIR)
	$(FSTAR) --codegen $(CODEGEN) \
	    --extract_module $(basename $(notdir $(subst _,.,$@))) \
	    --odir $(OCAML_DIR) \
	    $(notdir $(subst _,.,$(subst .ml,.fst,$@)))

$(OCAML_DIR)/Lib_RandomSequence.cmx: | $(OCAML_DIR)
	$(OCAMLOPT) $(FSTAR_HOME)/bin/fstarlib/fstarlib.cmxa $(HACL_HOME)/obj/libhaclml.cmxa -c $(HACL_HOME)/lib/ml/Lib_RandomSequence.ml -o $@

$(OCAML_DIR)/%.cmx: $(OCAML_DIR)/%.ml | $(OCAML_DIR)
	$(OCAMLOPT) -c $< -o $@

$(OCAML_DIR)/hacl.cmxa: $(HACL_CMX_FILES) $(VALE_CMX_FILES) | $(OCAML_DIR)
	ocamlfind opt -a -o $@ -package fstarlib -g -I $(OCAML_DIR) $^

# Clean targets

SHELL=/bin/bash

clean:
	rm -rf $(OCAML_DIR)/*

distclean: clean
	rm -rf $(CV2FSTAR_HOME)/.cache/*

