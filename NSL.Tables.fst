module NSL.Tables

open CVTypes
open NSL.Types
open State

let nsl_print_table_entry e =
  IO.print_string "{"; IO.print_newline ();
  match e with
  | TableEntry_trusted_keys var0 var1 var2 ->
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "skey" (serialize_skey var1) true;
    print_label_bytes  "pkey" (serialize_pkey var2) true;
    IO.print_string "}"; IO.print_newline ()
  | TableEntry_all_keys var0 var1 var2 ->
    print_label_bytes  "address" (serialize_address var0) true;
    print_label_bytes  "pkey" (serialize_pkey var1) true;
    print_label_bytes  "bool" (serialize_bool var2) true;
    IO.print_string "}"; IO.print_newline ()

let nsl_tablename_to_str t = match t with
  | Table_trusted_keys -> "trusted_keys"
  | Table_all_keys -> "all_keys"

let nsl_print_tables st =
  List.iter (fun e -> print_table st e (nsl_tablename_to_str e) (nsl_print_table_entry #e)) [Table_trusted_keys;Table_all_keys]