module NSL.Equations

open CVTypes
open NSL.Types
open NSL.Functions

val lemma_0: unit -> Lemma (
  forall (var_x1_0:skey) (var_x2_0:pkey) (var_x3_0:bool) (var_x4_0:lbytes 8) (var_x5_0:ciphertext) .
  (not (eq_msg1res (msg1succ var_x1_0 var_x2_0 var_x3_0 var_x4_0 var_x5_0) msg1fail)))

val lemma_1: unit -> Lemma (
  forall (var_z_0:lbytes 8) (var_t_0:lbytes 8) (var_u_0:address) (var_y2_0:lbytes 8) .
  (not (eq_plaintext (msg2 var_z_0 var_t_0 var_u_0) (msg3 var_y2_0))))

val lemma_2: unit -> Lemma (
  forall (var_y_0:lbytes 8) (var_y2_0:lbytes 8) (var_z2_0:address) .
  (not (eq_plaintext (msg3 var_y_0) (msg1 var_y2_0 var_z2_0))))

val lemma_3: unit -> Lemma (
  forall (var_z_0:lbytes 8) (var_t_0:lbytes 8) (var_u_0:address) (var_y2_0:lbytes 8) (var_z2_0:address) .
  (not (eq_plaintext (msg2 var_z_0 var_t_0 var_u_0) (msg1 var_y2_0 var_z2_0))))

val lemma_4: unit -> Lemma (
  forall (var_x_1:ciphertext) .
  (not (eq_ciphertext_opt (ciphertext_some var_x_1) ciphertext_bottom)))

val lemma_5: unit -> Lemma (
  forall (var_x_0:bool) (var_y_0:bool) (var_z_0:bool) .
  ((=) ((&&) (( || ) var_x_0 var_y_0) var_z_0) (( || ) ((&&) var_x_0 var_z_0) ((&&) var_y_0 var_z_0))))

val lemma_6: unit -> Lemma (
  forall (var_x_0:bool) .
  ((=) ((=) var_x_0 false) (not var_x_0)))

val lemma_7: unit -> Lemma (
  forall (var_x_0:bool) .
  ((=) (not ((=) var_x_0 true)) (not var_x_0)))

val lemma_8: unit -> Lemma (
  forall (var_x_0:bool) .
  ((=) (not ((=) var_x_0 false)) var_x_0))

val lemma_9: unit -> Lemma (
  forall (var_x_0:bool) .
  ((=) ((=) var_x_0 true) var_x_0))

val lemma_10: unit -> Lemma (
  ((=) (not false) true))

val lemma_11: unit -> Lemma (
  ((=) (not true) false))

val lemma_12: unit -> Lemma (
  forall (var_x_0:bool) (var_y_0:bool) .
  ((=) (not (( || ) var_x_0 var_y_0)) ((&&) (not var_x_0) (not var_y_0))))

val lemma_13: unit -> Lemma (
  forall (var_x_0:bool) (var_y_0:bool) .
  ((=) (not ((&&) var_x_0 var_y_0)) (( || ) (not var_x_0) (not var_y_0))))

val lemma_14: unit -> Lemma (
  forall (var_x_0:bool) .
  ((=) (not (not var_x_0)) var_x_0))

val lemma_15_inner (x0:lbytes 8) (x1:lbytes 8) (x2:address)  : Lemma (  match inv_msg2 (msg2 x0 x1 x2) with
  | Some (y0, y1, y2) ->
      eq_lbytes x0 y0 /\
      eq_lbytes x1 y1 /\
      eq_addr x2 y2
  | None -> False)
[SMTPat (inv_msg2 (msg2 x0 x1 x2))]

let lemma_15 () : Lemma (
  forall (x0:lbytes 8) (x1:lbytes 8) (x2:address) .
  match inv_msg2 (msg2 x0 x1 x2) with
  | Some (y0, y1, y2) ->
      eq_lbytes x0 y0 /\
      eq_lbytes x1 y1 /\
      eq_addr x2 y2
  | None -> False) = ()

val lemma_16_inner (x:plaintext): Lemma (  match inv_msg2 x   with
  | Some (y0, y1, y2) ->
      eq_plaintext (msg2 y0 y1 y2) x
  | None -> ~ (exists (y0:lbytes 8) (y1:lbytes 8) (y2:address) . eq_plaintext (msg2 y0 y1 y2) x))
[SMTPat (inv_msg2 x )]

let lemma_16 () : Lemma (
  forall (x:plaintext).
  match inv_msg2 x   with
  | Some (y0, y1, y2) ->
      eq_plaintext (msg2 y0 y1 y2) x
  | None -> ~ (exists (y0:lbytes 8) (y1:lbytes 8) (y2:address) . eq_plaintext (msg2 y0 y1 y2) x)) = ()

val lemma_17_inner (x0:skey) (x1:pkey) (x2:bool) (x3:lbytes 8) (x4:ciphertext)  : Lemma (  match inv_msg1succ (msg1succ x0 x1 x2 x3 x4) with
  | Some (y0, y1, y2, y3, y4) ->
      eq_skey x0 y0 /\
      eq_pkey x1 y1 /\
      (=) x2 y2 /\
      eq_lbytes x3 y3 /\
      eq_ciphertext x4 y4
  | None -> False)
[SMTPat (inv_msg1succ (msg1succ x0 x1 x2 x3 x4))]

let lemma_17 () : Lemma (
  forall (x0:skey) (x1:pkey) (x2:bool) (x3:lbytes 8) (x4:ciphertext) .
  match inv_msg1succ (msg1succ x0 x1 x2 x3 x4) with
  | Some (y0, y1, y2, y3, y4) ->
      eq_skey x0 y0 /\
      eq_pkey x1 y1 /\
      (=) x2 y2 /\
      eq_lbytes x3 y3 /\
      eq_ciphertext x4 y4
  | None -> False) = ()

val lemma_18_inner (x:msg1res): Lemma (  match inv_msg1succ x   with
  | Some (y0, y1, y2, y3, y4) ->
      eq_msg1res (msg1succ y0 y1 y2 y3 y4) x
  | None -> ~ (exists (y0:skey) (y1:pkey) (y2:bool) (y3:lbytes 8) (y4:ciphertext) . eq_msg1res (msg1succ y0 y1 y2 y3 y4) x))
[SMTPat (inv_msg1succ x )]

let lemma_18 () : Lemma (
  forall (x:msg1res).
  match inv_msg1succ x   with
  | Some (y0, y1, y2, y3, y4) ->
      eq_msg1res (msg1succ y0 y1 y2 y3 y4) x
  | None -> ~ (exists (y0:skey) (y1:pkey) (y2:bool) (y3:lbytes 8) (y4:ciphertext) . eq_msg1res (msg1succ y0 y1 y2 y3 y4) x)) = ()

val lemma_19_inner (x0:lbytes 8)  : Lemma (  match inv_msg3 (msg3 x0) with
  | Some (y0) ->
      eq_lbytes x0 y0
  | None -> False)
[SMTPat (inv_msg3 (msg3 x0))]

let lemma_19 () : Lemma (
  forall (x0:lbytes 8) .
  match inv_msg3 (msg3 x0) with
  | Some (y0) ->
      eq_lbytes x0 y0
  | None -> False) = ()

val lemma_20_inner (x:plaintext): Lemma (  match inv_msg3 x   with
  | Some (y0) ->
      eq_plaintext (msg3 y0) x
  | None -> ~ (exists (y0:lbytes 8) . eq_plaintext (msg3 y0) x))
[SMTPat (inv_msg3 x )]

let lemma_20 () : Lemma (
  forall (x:plaintext).
  match inv_msg3 x   with
  | Some (y0) ->
      eq_plaintext (msg3 y0) x
  | None -> ~ (exists (y0:lbytes 8) . eq_plaintext (msg3 y0) x)) = ()

val lemma_21_inner (x0:ciphertext)  : Lemma (  match inv_ciphertext_some (ciphertext_some x0) with
  | Some (y0) ->
      eq_ciphertext x0 y0
  | None -> False)
[SMTPat (inv_ciphertext_some (ciphertext_some x0))]

let lemma_21 () : Lemma (
  forall (x0:ciphertext) .
  match inv_ciphertext_some (ciphertext_some x0) with
  | Some (y0) ->
      eq_ciphertext x0 y0
  | None -> False) = ()

val lemma_22_inner (x:ciphertext_opt): Lemma (  match inv_ciphertext_some x   with
  | Some (y0) ->
      eq_ciphertext_opt (ciphertext_some y0) x
  | None -> ~ (exists (y0:ciphertext) . eq_ciphertext_opt (ciphertext_some y0) x))
[SMTPat (inv_ciphertext_some x )]

let lemma_22 () : Lemma (
  forall (x:ciphertext_opt).
  match inv_ciphertext_some x   with
  | Some (y0) ->
      eq_ciphertext_opt (ciphertext_some y0) x
  | None -> ~ (exists (y0:ciphertext) . eq_ciphertext_opt (ciphertext_some y0) x)) = ()

val lemma_23_inner (x0:lbytes 8) (x1:address)  : Lemma (  match inv_msg1 (msg1 x0 x1) with
  | Some (y0, y1) ->
      eq_lbytes x0 y0 /\
      eq_addr x1 y1
  | None -> False)
[SMTPat (inv_msg1 (msg1 x0 x1))]

let lemma_23 () : Lemma (
  forall (x0:lbytes 8) (x1:address) .
  match inv_msg1 (msg1 x0 x1) with
  | Some (y0, y1) ->
      eq_lbytes x0 y0 /\
      eq_addr x1 y1
  | None -> False) = ()

val lemma_24_inner (x:plaintext): Lemma (  match inv_msg1 x   with
  | Some (y0, y1) ->
      eq_plaintext (msg1 y0 y1) x
  | None -> ~ (exists (y0:lbytes 8) (y1:address) . eq_plaintext (msg1 y0 y1) x))
[SMTPat (inv_msg1 x )]

let lemma_24 () : Lemma (
  forall (x:plaintext).
  match inv_msg1 x   with
  | Some (y0, y1) ->
      eq_plaintext (msg1 y0 y1) x
  | None -> ~ (exists (y0:lbytes 8) (y1:address) . eq_plaintext (msg1 y0 y1) x)) = ()

val lemma_25_inner (x0:plaintext)  : Lemma (  match inv_injbot (injbot x0) with
  | Some (y0) ->
      eq_plaintext x0 y0
  | None -> False)
[SMTPat (inv_injbot (injbot x0))]

let lemma_25 () : Lemma (
  forall (x0:plaintext) .
  match inv_injbot (injbot x0) with
  | Some (y0) ->
      eq_plaintext x0 y0
  | None -> False) = ()

val lemma_26_inner (x:option bytes): Lemma (  match inv_injbot x   with
  | Some (y0) ->
      eq_obytes (injbot y0) x
  | None -> ~ (exists (y0:plaintext) . eq_obytes (injbot y0) x))
[SMTPat (inv_injbot x )]

let lemma_26 () : Lemma (
  forall (x:option bytes).
  match inv_injbot x   with
  | Some (y0) ->
      eq_obytes (injbot y0) x
  | None -> ~ (exists (y0:plaintext) . eq_obytes (injbot y0) x)) = ()

val lemma_27_inner (x0:pkey) (x1:skey)  : Lemma (  match inv_kp (kp x0 x1) with
  | Some (y0, y1) ->
      eq_pkey x0 y0 /\
      eq_skey x1 y1
  | None -> False)
[SMTPat (inv_kp (kp x0 x1))]

let lemma_27 () : Lemma (
  forall (x0:pkey) (x1:skey) .
  match inv_kp (kp x0 x1) with
  | Some (y0, y1) ->
      eq_pkey x0 y0 /\
      eq_skey x1 y1
  | None -> False) = ()

val lemma_28_inner (x:keypair): Lemma (  match inv_kp x   with
  | Some (y0, y1) ->
      eq_keypair (kp y0 y1) x
  | None -> ~ (exists (y0:pkey) (y1:skey) . eq_keypair (kp y0 y1) x))
[SMTPat (inv_kp x )]

let lemma_28 () : Lemma (
  forall (x:keypair).
  match inv_kp x   with
  | Some (y0, y1) ->
      eq_keypair (kp y0 y1) x
  | None -> ~ (exists (y0:pkey) (y1:skey) . eq_keypair (kp y0 y1) x)) = ()

val lemma_29_inner (a:ciphertext) : Lemma (  match deserialize_ciphertext (serialize_ciphertext a) with
  | Some b -> eq_ciphertext a b
  | None -> False)
[SMTPat (deserialize_ciphertext (serialize_ciphertext a))]

let lemma_29 () : Lemma (
  forall (a:ciphertext).
  match deserialize_ciphertext (serialize_ciphertext a) with
  | Some b -> eq_ciphertext a b
  | None -> False) = ()

val lemma_30_inner (a:bytes) : Lemma (  match deserialize_ciphertext a with
  | Some b -> eq_bytes (serialize_ciphertext b) a
  | None -> ~ (exists (b:ciphertext). eq_bytes (serialize_ciphertext b) a))
[SMTPat (deserialize_ciphertext a)]

let lemma_30 () : Lemma (
  forall (a:bytes).
  match deserialize_ciphertext a with
  | Some b -> eq_bytes (serialize_ciphertext b) a
  | None -> ~ (exists (b:ciphertext). eq_bytes (serialize_ciphertext b) a)) = ()

val lemma_31_inner (a:skey) : Lemma (  match deserialize_skey (serialize_skey a) with
  | Some b -> eq_skey a b
  | None -> False)
[SMTPat (deserialize_skey (serialize_skey a))]

let lemma_31 () : Lemma (
  forall (a:skey).
  match deserialize_skey (serialize_skey a) with
  | Some b -> eq_skey a b
  | None -> False) = ()

val lemma_32_inner (a:bytes) : Lemma (  match deserialize_skey a with
  | Some b -> eq_bytes (serialize_skey b) a
  | None -> ~ (exists (b:skey). eq_bytes (serialize_skey b) a))
[SMTPat (deserialize_skey a)]

let lemma_32 () : Lemma (
  forall (a:bytes).
  match deserialize_skey a with
  | Some b -> eq_bytes (serialize_skey b) a
  | None -> ~ (exists (b:skey). eq_bytes (serialize_skey b) a)) = ()

val lemma_33_inner (a:pkey) : Lemma (  match deserialize_pkey (serialize_pkey a) with
  | Some b -> eq_pkey a b
  | None -> False)
[SMTPat (deserialize_pkey (serialize_pkey a))]

let lemma_33 () : Lemma (
  forall (a:pkey).
  match deserialize_pkey (serialize_pkey a) with
  | Some b -> eq_pkey a b
  | None -> False) = ()

val lemma_34_inner (a:bytes) : Lemma (  match deserialize_pkey a with
  | Some b -> eq_bytes (serialize_pkey b) a
  | None -> ~ (exists (b:pkey). eq_bytes (serialize_pkey b) a))
[SMTPat (deserialize_pkey a)]

let lemma_34 () : Lemma (
  forall (a:bytes).
  match deserialize_pkey a with
  | Some b -> eq_bytes (serialize_pkey b) a
  | None -> ~ (exists (b:pkey). eq_bytes (serialize_pkey b) a)) = ()

val lemma_35_inner (a:address) : Lemma (  match deserialize_address (serialize_address a) with
  | Some b -> eq_addr a b
  | None -> False)
[SMTPat (deserialize_address (serialize_address a))]

let lemma_35 () : Lemma (
  forall (a:address).
  match deserialize_address (serialize_address a) with
  | Some b -> eq_addr a b
  | None -> False) = ()

val lemma_36_inner (a:bytes) : Lemma (  match deserialize_address a with
  | Some b -> eq_bytes (serialize_address b) a
  | None -> ~ (exists (b:address). eq_bytes (serialize_address b) a))
[SMTPat (deserialize_address a)]

let lemma_36 () : Lemma (
  forall (a:bytes).
  match deserialize_address a with
  | Some b -> eq_bytes (serialize_address b) a
  | None -> ~ (exists (b:address). eq_bytes (serialize_address b) a)) = ()

val lemma_37: unit -> Lemma (
  forall (a:bool) (b:bool) . (=) ((&&) a b) ((&&) b a))

val lemma_38: unit -> Lemma (
  forall (a:bool) (b:bool) (c:bool) . (=) ((&&) a ((&&) b c)) ((&&) ((&&) a b) c))

val lemma_39: unit -> Lemma (
  forall (a:bool) . ((=) ((&&) a true) a) /\ ((=)((&&) true a) a))

val lemma_40: unit -> Lemma (
  forall (a:bool) (b:bool) . (=) (( || ) a b) (( || ) b a))

val lemma_41: unit -> Lemma (
  forall (a:bool) (b:bool) (c:bool) . (=) (( || ) a (( || ) b c)) (( || ) (( || ) a b) c))

val lemma_42: unit -> Lemma (
  forall (a:bool) . ((=) (( || ) a false) a) /\ ((=)(( || ) false a) a))

val lemma_43: unit -> Lemma (
  forall (a:bool) (b:bool) . (=) ((=) a b) ((=) b a))

val lemma_44: unit -> Lemma (
  forall (a:bool) (b:bool) . (=) (not ((=) a b)) (not ((=) b a)))

val lemma_45: unit -> Lemma (
  forall (a:bytes) (b:bytes) . (=) (eq_bytes a b) (eq_bytes b a))

val lemma_46: unit -> Lemma (
  forall (a:bytes) (b:bytes) . (=) (not (eq_bytes a b)) (not (eq_bytes b a)))

val lemma_47: unit -> Lemma (
  forall (a:option bytes) (b:option bytes) . (=) (eq_obytes a b) (eq_obytes b a))

val lemma_48: unit -> Lemma (
  forall (a:option bytes) (b:option bytes) . (=) (not (eq_obytes a b)) (not (eq_obytes b a)))

val lemma_49: unit -> Lemma (
  forall (a:ciphertext_opt) (b:ciphertext_opt) . (=) (not (eq_ciphertext_opt a b)) (not (eq_ciphertext_opt b a)))

val lemma_50: unit -> Lemma (
  forall (a:plaintext) (b:plaintext) . (=) (not (eq_plaintext a b)) (not (eq_plaintext b a)))

val lemma_51: unit -> Lemma (
  forall (a:msg1res) (b:msg1res) . (=) (not (eq_msg1res a b)) (not (eq_msg1res b a)))

